package com.ingetorres.movieapp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingetorres.movieapp.R;

public class SplashScreen extends AppCompatActivity {

    private ImageView imageScreen;
    private TextView textName;
    private TextView textPresent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        imageScreen = (ImageView) findViewById(R.id.imageScreen);
        textName = (TextView) findViewById(R.id.textName);
        textPresent = (TextView) findViewById(R.id.textPresent);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.transition);
        imageScreen.startAnimation(animation);
        textName.startAnimation(animation);
        textPresent.startAnimation(animation);

        Thread splash = new Thread(){
            public void run(){
                try{
                    sleep(2000);

                    Intent home = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(home);

                    finish();
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        };splash.start();


    }
}
