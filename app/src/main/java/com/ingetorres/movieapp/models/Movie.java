package com.ingetorres.movieapp.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by juantorres on 9/11/17.
 */

public class Movie implements Serializable {

    public static final String URL_IMAGE = "https://image.tmdb.org/t/p/w185_and_h278_bestv2/";

    private int id;
    private String original_title;
    private String poster_path;
    private double vote_average;
    private String release_date;
    private String overview;
    private String key;
    private int budget;
    private int vote_count;
    private ArrayList<Movie> results;

    public Movie(int id, String original_title, String poster_path, double vote_average, String release_date, String overview, String key, int budget, int vote_count) {
        this.id = id;
        this.original_title = original_title;
        this.poster_path = poster_path;
        this.vote_average = vote_average;
        this.release_date = release_date;
        this.overview = overview;
        this.key = key;
        this.budget = budget;
        this.vote_count = vote_count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOriginal_title() {
        return original_title;
    }

    public void setOriginal_title(String original_title) {
        this.original_title = original_title;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public double getVote_average() {
        return vote_average;
    }

    public void setVote_average(double vote_average) {
        this.vote_average = vote_average;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public int getVote_count() {
        return vote_count;
    }

    public void setVote_count(int vote_count) {
        this.vote_count = vote_count;
    }

    public ArrayList<Movie> getResults() {
        return results;
    }

    public void setResults(ArrayList<Movie> results) {
        this.results = results;
    }
}
