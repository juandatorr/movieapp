package com.ingetorres.movieapp.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.ingetorres.movieapp.R;
import com.ingetorres.movieapp.adapters.MyAdapter;
import com.ingetorres.movieapp.constantes.Constantes;
import com.ingetorres.movieapp.models.Movie;
import com.ingetorres.movieapp.service.MovieService;
import com.ingetorres.movieapp.service.ServiceRetro;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements Callback<Movie> {


    private ArrayList<Movie> movies;
    private ArrayList<Movie> movSearch;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layout;
    private ServiceRetro service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        service = new ServiceRetro();
        Constantes.key = getSharedPreferences("key", Context.MODE_PRIVATE);
        Constantes.saveOnPreferences();
        movies = new ArrayList<Movie>();
        movSearch = new ArrayList<Movie>();
        callRetrofit();

    }

    public void callRetrofit() {
        Call<Movie> movieCall = service.getService().getMovie(Constantes.key.getString("key", "key"), "popularity.desc");
        movieCall.enqueue(this);
    }

    public Movie getMovie(Movie movie){
        Movie newMovie = new Movie(movie.getId(), movie.getOriginal_title(), movie.getPoster_path(), movie.getVote_average(), movie.getRelease_date(), movie.getOverview(), movie.getKey(), movie.getBudget(), movie.getVote_count());
        return newMovie;
    }

    @Override
    public void onResponse(Call<Movie> call, Response<Movie> response) {

        for(int i = 0; i < response.body().getResults().size(); i++) {
            movies.add(getMovie(response.body().getResults().get(i)));
        }
        cardView(movies);
    }

    @Override
    public void onFailure(Call<Movie> call, Throwable t) {

    }

    public void cardView(ArrayList<Movie> mov){
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        layout = new LinearLayoutManager(this);

        adapter = new MyAdapter(mov, R.layout.activity_card_view, this, new MyAdapter.OnItemClickListener() {
            @Override
            public void onItemClickListener(Movie movie, int posicion) {
                adapter.notifyItemChanged(posicion);
                Intent intent = new Intent(MainActivity.this, MovieActivity.class);
                intent.putExtra("Movie", movie);
                startActivity(intent);
            }
        });

        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setLayoutManager(layout);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.searchMovies:
                movSearch.clear();
                search();
                adapter.notifyDataSetChanged();
                return true;

            case R.id.home:
                movies.clear();
                callRetrofit();
                adapter.notifyDataSetChanged();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    public void search(){
        Call<Movie> movieCall = service.getService().getMovieCount(Constantes.key.getString("key", "key"), "vote_count.asc", "2000");
        movieCall.enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                for(int i = 0; i < response.body().getResults().size(); i++) {
                    movSearch.add(getMovie(response.body().getResults().get(i)));
                }

                cardView(movSearch);
            }
            @Override
            public void onFailure(Call<Movie> call, Throwable t) {

            }
        });
    }
}
