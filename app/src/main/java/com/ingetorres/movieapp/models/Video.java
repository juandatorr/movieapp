package com.ingetorres.movieapp.models;

import java.util.ArrayList;

/**
 * Created by juantorres on 9/11/17.
 */

public class Video {

   private String key;
   private ArrayList<Video> results;


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public ArrayList<Video> getResults() {
        return results;
    }


    public Video(String key) {

        this.key = key;
    }
}
