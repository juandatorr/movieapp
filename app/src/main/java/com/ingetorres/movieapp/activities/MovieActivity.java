package com.ingetorres.movieapp.activities;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ingetorres.movieapp.R;
import com.ingetorres.movieapp.constantes.Constantes;
import com.ingetorres.movieapp.models.Movie;
import com.ingetorres.movieapp.models.Video;
import com.ingetorres.movieapp.service.ServiceRetro;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imagenViewMovie;
    private TextView textViewName;
    private TextView textOverview;
    private TextView textReleaseDate;
    private TextView textBudget;
    private TextView textTrailer;
    private ImageButton imageButton;

    private String keyMovie;
    private ServiceRetro serviceRetro;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        serviceRetro = new ServiceRetro();
        Movie movieId = (Movie) getIntent().getSerializableExtra("Movie");
        init();
        callMovieVideo(movieId);
        callMovieId(movieId);
        Constantes.key = getSharedPreferences("key", Context.MODE_PRIVATE);
        Constantes.saveOnPreferences();
    }


    public void init(){
        imagenViewMovie = (ImageView) findViewById(R.id.imageViewMovie);
        textViewName = (TextView) findViewById(R.id.textViewName);
        textOverview = (TextView) findViewById(R.id.textViewOver);
        textReleaseDate = (TextView) findViewById(R.id.textViewRelease);
        textBudget = (TextView) findViewById(R.id.textViewBudget);
        textTrailer = (TextView) findViewById(R.id.textViewKey);
        imageButton = (ImageButton) findViewById(R.id.imageButton);
        imageButton.setOnClickListener(this);
    }

    public void callMovieId(Movie movie){
        String id = String.valueOf(movie.getId());
        Call<Movie> callMovieNew =  serviceRetro.getService().getMovieId(id, Constantes.key.getString("key", "key"));
        callMovieNew.enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                Movie movie = response.body();
                setAtributes(movie);
            }

            @Override
            public void onFailure(Call<Movie> call, Throwable t) {

            }
        });
    }

    public void callMovieVideo(Movie movie) {
        String id = String.valueOf(movie.getId());
        Call<Video> callNew = serviceRetro.getService().getVideo(id, Constantes.key.getString("key", "key"));

        callNew.enqueue(new Callback<Video>() {
            @Override
            public void onResponse(Call<Video> call, Response<Video> response) {
                for (int i=0; i < response.body().getResults().size(); i++){
                    setVideo(response.body().getResults().get(i).getKey());
                }
            }

            @Override
            public void onFailure(Call<Video> call, Throwable t) {

            }
        });
    }
    public void setAtributes(Movie movie){
        if(movie != null) {
            Picasso.with(getApplicationContext()).load(Movie.URL_IMAGE + movie.getPoster_path()).fit().into(imagenViewMovie);
            textViewName.setText(movie.getOriginal_title());
            textReleaseDate.setText(movie.getRelease_date());
            textOverview.setText(movie.getOverview());
            textOverview.setMovementMethod(new ScrollingMovementMethod());
            textBudget.setText(Constantes.formateador.format(movie.getBudget()));
        }
    }

    public void setVideo(String movieKey){
        if(movieKey != null){
            keyMovie = movieKey;
            textTrailer.setText("Youtube");
        }else{
            textTrailer.setText("No video.");
        }
    }

    public void watchVideo(String key){
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + key));
        Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=" + key));
        try {
            startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            startActivity(webIntent);
    }

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == imageButton.getId()){
            watchVideo(keyMovie);
        }
    }



}
