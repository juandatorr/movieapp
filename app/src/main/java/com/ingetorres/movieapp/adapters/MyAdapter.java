package com.ingetorres.movieapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;

import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingetorres.movieapp.R;
import com.ingetorres.movieapp.models.Movie;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by juantorres on 2/08/17.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private List<Movie> movies;
    private int layout;
    private Activity activity;
    private OnItemClickListener onItemClickListener;
    private Context context;

    //Inflamos el layoutInflater para pasarlo a nuestro ViewHolder
    public MyAdapter(List<Movie> movies, int layout, Activity activity, OnItemClickListener onItemClickListener) {
        this.movies = movies;
        this.layout = layout;
        this.onItemClickListener = onItemClickListener;
        this.activity = activity;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        context = parent.getContext();
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    //Llamamos el metodo bind que creamos en la clase ViewHolder
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(movies.get(position), onItemClickListener);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    //Clase que maneja las referencias de cada uno de los items del layouts
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener, MenuItem.OnMenuItemClickListener{

        private ImageView imagenViewMovie;
        private TextView textNombreMovie;
        private TextView textViewAverege;



        public ViewHolder(View view) {
            super(view);
            imagenViewMovie = (ImageView) view.findViewById(R.id.imageViewMovie);
            textNombreMovie = (TextView) view.findViewById(R.id.textViewNombre);
            textViewAverege = (TextView) view.findViewById(R.id.textViewAverage);
            itemView.setOnCreateContextMenuListener(this);

            }


        //Metodo que asigna al layout los valores correspondientes
        public void bind(final Movie movie, final OnItemClickListener onItemClickListener) {
            Picasso.with(context).load(Movie.URL_IMAGE + movie.getPoster_path()).fit().into(imagenViewMovie);
            textNombreMovie.setText(movie.getOriginal_title());
            textViewAverege.setText(String.valueOf(movie.getVote_average()));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onItemClickListener(movie, getAdapterPosition());
                }
            });


        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            return false;
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        }
    }



    //Evento de cada cardView que este dentro del layout
    public interface OnItemClickListener {
        void onItemClickListener(Movie movie, int posicion);
    }


    public interface OnItemLongClickListener{
        void onItemLongClick(int position);
    }



}
