package com.ingetorres.movieapp.service;

import com.ingetorres.movieapp.models.Movie;
import com.ingetorres.movieapp.models.Video;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by juantorres on 9/11/17.
 */

public interface MovieService {

    //https://api.themoviedb.org/3/discover/movie?api_key=13880361cc0acb9f388f732c1c31592d&sort_by=popularity.desc
    @GET("discover/movie")
    Call<Movie> getMovie(@Query("api_key") String key, @Query("sort_by") String sort);

    //https://api.themoviedb.org/3/movie/346364/images?api_key=13880361cc0acb9f388f732c1c31592d&language=es
    @GET("movie/{movie_id}/videos")
    Call<Video> getVideo(@Path("movie_id") String id, @Query("api_key") String key);

    @GET("movie/{id}")
    Call<Movie> getMovieId(@Path("id") String id,  @Query("api_key") String key);

    //https://api.themoviedb.org/3/discover/movie?api_key=13880361cc0acb9f388f732c1c31592d&sort_by=vote_count.desc&vote_count.gte=2000
    @GET("discover/movie")
    Call<Movie> getMovieCount(@Query("api_key") String key, @Query("sort_by") String sort, @Query("vote_count.gte") String vote);

}
